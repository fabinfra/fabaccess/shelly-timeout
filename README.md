# Shelly Timeout

Shelly Initiator with timeout on power consumption. Detailed installation and usage instructions can be found at [docs.fab-access.org](https://docs.fab-access.org/books/plugins-aktoren-initiatoren/page/initiator-shelly-timeout)